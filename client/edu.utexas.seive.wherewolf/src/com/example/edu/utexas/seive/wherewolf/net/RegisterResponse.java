package com.example.edu.utexas.seive.wherewolf.net;

public class RegisterResponse extends BasicResponse{
	  
	  private int playerID = -1;

	  public RegisterResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public RegisterResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	      this.playerID = playerID;
	  }

	  
	  public int getPlayerID()
	  {
	      return playerID;
	  }
	  
	}
