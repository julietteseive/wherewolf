package com.example.edu.utexas.seive.wherewolf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

	public class CircadianWidgetView extends View {
		private static final String TAG = "mainscreenactivity";
		private Paint canvasPaint, drawPaint;
		private Bitmap canvasBitmap, moonBitmap, sunBitmap, nightBitmap, dayBitmap, dusk2Bitmap, dusk1Bitmap;
		private Canvas drawCanvas;
		double currentTime;
	
	public CircadianWidgetView(Context context) {
		super(context);
		initPaint();
	}
	
	public CircadianWidgetView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPaint();
		
	}
	
	private void initPaint() {
		drawPaint = new Paint();
		canvasPaint = new Paint(Paint.DITHER_FLAG);
		// be sure that you have pngs or jpgs in your drawables folder with 
		// the corresponding names (moon, night, etc)
		sunBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sun);
		nightBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.night);
		moonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.moon3);
		dayBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.day);
		dusk2Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dusk2);
		dusk1Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dusk1);
		
	}
	
	public void changeTime(double time) {
		currentTime = time;
		Log.i(TAG, "before invalidate");
		invalidate(); // causes the onDraw method to be invoked
		Log.i(TAG, "after invalidate");
	}
	
	protected void onDraw(Canvas canvas) {
		Log.i(TAG, "drawing");
		super.onDraw(canvas);
		double w = drawCanvas.getWidth();
		double h = drawCanvas.getHeight();

		int iW = moonBitmap.getWidth() / 2;
		int iH = moonBitmap.getHeight() / 2;
		
		int sW = sunBitmap.getWidth() / 2;
		int sH = sunBitmap.getHeight() /2;
		
		Log.i(TAG, "its the ifs");
		// draw the backdrop here
		if (currentTime >= 5.0 && currentTime < 6.0) {
		drawCanvas.drawBitmap(dusk1Bitmap, 
			    0, 0, drawPaint);
		Log.i(TAG, "hello");
		}
		else if (currentTime >= 17.0 && currentTime < 18.0) {
		drawCanvas.drawBitmap(dusk2Bitmap, 
			    0, 0, drawPaint);
		}
		
		else if (currentTime >= 6.0 && currentTime < 17.0) {
			drawCanvas.drawBitmap(dayBitmap, 
				    0, 0, drawPaint);
		}
		
		else {
			drawCanvas.drawBitmap(nightBitmap, 
				    0, 0, drawPaint);
		}
		
		Log.i(TAG, "ifs are okay");
		
		// calculate the angle the moon should appear in the sky
		double theta = Math.PI / 2 + Math.PI * currentTime / 12;
		
		double thetasun = theta + Math.PI;

		// calculate the x and y coordinates of where to draw the images
		// keep in mind the coordinates are the top left of the images
		// so you can use the bitmap width and height to compensate.

		double moonPosX = w / 2 - w / 3 * Math.cos(theta);
	    double moonPosY = h / 2 - h / 3 * Math.sin(theta) + h/6; // replace this with your value
	
	    double sunPosX = w / 2 - w / 3 * Math.cos(thetasun);
	    double sunPosY = h / 2 - h / 3 * Math.sin(thetasun) + h/6; // replace this with your value
	    
	    drawCanvas.drawBitmap(moonBitmap, 
	    (int) moonPosX - iW, (int) moonPosY + iH, drawPaint); 
	
	drawCanvas.drawBitmap(sunBitmap, 
	(int) sunPosX - sW, (int) sunPosY + sH, drawPaint);

		// draw your sun and other things here as well.
		// experiment with drawCanvas.drawText for putting labels of whether it is day
		// or night.

		// you need to actually move the offscreen bitmap to the on-screen bitmap
		canvas.drawBitmap(canvasBitmap, 0, 0, drawPaint);
		Log.i(TAG, "hihihihi");
	}
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}
}


