package com.example.edu.utexas.seive.wherewolf.net;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.example.edu.utexas.seive.wherewolf.net.WherewolfNetworking;

public class SignInRequest extends BasicRequest {
	private static final String TAG = "signinrequest";

  public SignInRequest (String username, String password)
  {
      super(username, password);
  }
  
 
  String host = "http://54.69.96.208:5000/v1";
  
  @Override
  public String getURL() {
      return "/authenticate";
  }

  @Override
  public List<NameValuePair> getParameters() {
      return null;
  }

  @Override
  public RequestType getRequestType() {
      return RequestType.GET;
  }

  @Override
  public SignInResponse execute(WherewolfNetworking net) {
  
      try {
          JSONObject response = net.sendRequest(this);
          Log.v(TAG, "Connecting");
          if (response.getString("status").equals("success"))
          {
              //int playerID = response.getInt("playerid");
              return new SignInResponse("success", "signed in successfully");
          } else {
              
              String errorMessage = response.getString("status");
              return new SignInResponse("failure", errorMessage);
          }
      } catch (JSONException e) {
          return new SignInResponse("failure", "sign in not working");
      } catch (WherewolfNetworkException ex)
      {
          return new SignInResponse("failure", "could not communicate with the server");
      }
      
      
      
  }

}