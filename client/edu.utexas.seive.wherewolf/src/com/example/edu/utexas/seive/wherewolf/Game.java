package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;

public class Game {
	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	private int gameId;
	private String gameName;
	private String adminName;
	
	public Game(int gameId, String gameName, String adminName) {
		this.gameId = gameId;
		this.gameName = gameName;
		this.adminName = adminName;
	}
		
	public static ArrayList<Game> getGame(){
	ArrayList<Game> arrayOfGames = new ArrayList<Game>();
	arrayOfGames.add(new Game(1, "examplegame1", "Riley"));
	arrayOfGames.add(new Game(2, "examplegame2", "Juliette"));
	arrayOfGames.add(new Game(3, "examplegame3", "Sven"));
	return arrayOfGames;
	}
}
