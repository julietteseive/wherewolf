
package com.example.edu.utexas.seive.wherewolf.net;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.edu.utexas.seive.wherewolf.GameSelectionActivity;
import com.example.edu.utexas.seive.wherewolf.net.BasicRequest.RequestType;

import android.util.Base64;
import android.util.Log;

public class WherewolfNetworking {

	  private static final String TAG = "WherewolfNetworking";
	  private static final String fullhost = "http://54.69.96.208:5000/v1";
	  
	  public WherewolfNetworking() {

	  }

	  public JSONObject sendRequest(BasicRequest basicRequest)
	          throws WherewolfNetworkException {
	      
	      InputStream inputStream = null;
	      String result = null;
	      Log.v(TAG, "Sending Request");
	      String url = fullhost + basicRequest.getURL();
	      Log.v(TAG, url);
	      RequestType requestType = basicRequest.getRequestType();
	      Log.v(TAG, "Hello");
	      List<NameValuePair> payload = basicRequest.getParameters();
	      String username = basicRequest.getUsername();
	      Log.v(TAG, username);
	      String password = basicRequest.getPassword();
	      Log.v(TAG, password);
	      try {

	          final DefaultHttpClient httpClient = new DefaultHttpClient();

	          // HttpUriRequest request;
	          HttpResponse response;

	          HttpUriRequest request;

	          if (basicRequest.getRequestType() == RequestType.GET) {
	              request = new HttpGet(url);
	              Log.v(TAG, "here");
	              request.setHeader("Content-type", "application/json");

	          } else if (requestType == RequestType.POST) {
	        	  Log.v(TAG, "Or here");
	              HttpPost postRequest = new HttpPost(url);
	              postRequest.setHeader("Content-type",
	                      "application/x-www-form-urlencoded");
	              Log.v(TAG, "made it here");
	              if (payload!=null) {
	            	  Log.v(TAG, "payload");
	                  postRequest.setEntity(new UrlEncodedFormEntity(payload));
	              }
	              Log.v(TAG, "post pay");
	              request = postRequest;
	              

	          } else if (requestType == RequestType.DELETE) {
	              
	              HttpDelete deleteRequest = new HttpDelete(url);
	          
	              request = deleteRequest;
	              request.setHeader("Content-type", "application/json");
	              
	          } else if (requestType == RequestType.PUT) {
	              
	              request = new HttpPut(url);
	              request.setHeader("Content-type", "application/json");
	              
	                  
	          } else {
	              throw new WherewolfNetworkException(
	                      "Does not support the HTTP request type");
	          }

	          
	          
	          

	          if (!username.equals("")) {
	              String authorizationString = "Basic "
	                      + Base64.encodeToString(
	                              (username + ":" + password).getBytes(),
	                              Base64.NO_WRAP);
	              request.setHeader("Authorization", authorizationString);
	          }
	          Log.v(TAG, "checking");
	          response = httpClient.execute(request);
	          Log.v(TAG, "entity");
	          HttpEntity entity = response.getEntity();
	          Log.v(TAG, "last one");
	          inputStream = entity.getContent();

	          BufferedReader reader = new BufferedReader(new InputStreamReader(
	                  inputStream, "UTF-8"), 8);
	          StringBuilder sb = new StringBuilder();

	          String line = null;
	          while ((line = reader.readLine()) != null) {
	              sb.append(line + "\n");
	          }

	          result = sb.toString();

	          try {

	              JSONObject json = new JSONObject(result);
	              return json;

	          } catch (JSONException ex) {
	             throw new WherewolfNetworkException("");
	          }

	      } catch (Exception e) {
	          Log.e(TAG, "Problem with response from server" + e.toString());

	      } finally {
	          try {
	              if (inputStream != null)
	                  inputStream.close();
	          } catch (Exception ex) {
	              throw new WherewolfNetworkException("Network problem");
	          }
	      }

	      throw new WherewolfNetworkException("Network problem");

	  }
}