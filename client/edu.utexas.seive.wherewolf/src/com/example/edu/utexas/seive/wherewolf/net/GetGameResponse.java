package com.example.edu.utexas.seive.wherewolf.net;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.edu.utexas.seive.wherewolf.Game;

import android.util.Log;

public class GetGameResponse extends BasicResponse {
	  private static final String TAG = "getgameactivity";
	
	  private int playerID = -1;
	  private ArrayList<Game> allGames = new ArrayList<Game>();
	  private int game_id;
	  private String game_name;
	  private String game_status;
	  
	  public GetGameResponse(String status, String errorMessage) {
		  super(status, errorMessage);
	  }
	  
	  public GetGameResponse(String status, String stringError, JSONArray games) {
	     super(status, stringError);
	     try{
	      Log.v(TAG, "we out here trying");
	      Log.v(TAG, "length of games in response: "+""+games.length());
	      for (int i = 0; i < games.length(); i++) {
	    	  game_id = games.getJSONObject(i).getInt("game_id");
	    	  game_name= games.getJSONObject(i).getString("name");
	    	  game_status = games.getJSONObject(i).getString("status");
	    	  allGames.add(new Game(game_id, game_name, ""+game_id));
	      }
	     }
	     catch (JSONException e) {
	    	 Log.v(TAG, "JSON exception caught");
	          int b = 5;
	      } 
	  }
	  
	  public GetGameResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	      this.playerID = playerID;
	  }

	  
	  public int getPlayerID()
	  {
	      return playerID;
	  }
	  
	  public ArrayList<Game> getGameInfo()
	  {
	      return allGames;
	  }
	  
	}
