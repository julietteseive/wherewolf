package com.example.edu.utexas.seive.wherewolf.net;

public class WherewolfNetworkException extends Exception {

	private static final long serialVersionUID = 1L;

	  public WherewolfNetworkException(String message) {
	      super(message);
	  }
}
