package com.example.edu.utexas.seive.wherewolf;


import com.example.edu.utexas.seive.wherewolf.net.SignInRequest;
import com.example.edu.utexas.seive.wherewolf.net.SignInResponse;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfNetworking;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {

	private static final String TAG = "loginactivity";
	
	private class SignInTask extends AsyncTask<Void, Integer, SignInResponse> {
		
		private static final String TAG = "loginactivity";
		

	    @Override
	    protected SignInResponse doInBackground(Void... request) {

	        final EditText nameTV = (EditText) findViewById(R.id.usernameText);
	        final EditText passTV = (EditText) findViewById(R.id.passwordText);
	        
	        String username = nameTV.getText().toString();
	        String password = passTV.getText().toString();
	        
	        SignInRequest signinRequest = new SignInRequest(username, password);
	        
	        return signinRequest.execute(new WherewolfNetworking());
	        
	    
	    }

	    protected void onPostExecute(SignInResponse result) {

	        Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
	        
	        final TextView errorText = (TextView) findViewById(R.id.error_text);
	        
	        if (result.getStatus().equals("success")) {
	                        
	            final EditText nameTV = (EditText) findViewById(R.id.usernameText);
	            final EditText passTV = (EditText) findViewById(R.id.passwordText);
	            
	            String username = nameTV.getText().toString();
		        String password = passTV.getText().toString();
	            
	            WherewolfPreferences pref = new WherewolfPreferences(LoginActivity.this);
	            pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

	            errorText.setText("");
	            Log.v(TAG, "Signing in");
	           
	            
	            startGameSelectionActivity();
	            //Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
	            //startActivity(intent);
	            //overridePendingTransition(R.anim.slide_in_right,
	            //        R.anim.slide_out_right);
	        } else {
	            // do something with bad password
	            
	            errorText.setText(result.getErrorMessage());
	        }

	    }

	    

	}
	
	private int startRegisterActivity()
	{
		Log.v(TAG, "User pressed the register button");
		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
		return 8;
	}
	
	private int startGameSelectionActivity()
	{
		Log.v(TAG, "User pressed the login button");
		Intent intent = new Intent(this, GameSelectionActivity.class);
		startActivity(intent);
		return 8;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Log.i(TAG, "created the login activity");

		final Button button = (Button) findViewById(R.id.registerButton);

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				startRegisterActivity();
			}	
			
		};

		final Button button2 = (Button) findViewById(R.id.loginButton);

		View.OnClickListener pim = new View.OnClickListener() {
			public void onClick(View v) {
				new SignInTask().execute();
			}
		};
		button.setOnClickListener(jim);
		button2.setOnClickListener(pim);
		/*
		 * button.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { // Perform action on click } });
		 */		
	}

	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.login, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * action bar item clicks here. The action bar will // automatically handle
	 * clicks on the Home/Up button, so long // as you specify a parent activity
	 * in AndroidManifest.xml. int id = item.getItemId(); if (id ==
	 * R.id.action_settings) { return true; } return
	 * super.onOptionsItemSelected(item); }
	 */
}