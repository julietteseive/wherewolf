package com.example.edu.utexas.seive.wherewolf.net;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.example.edu.utexas.seive.wherewolf.net.BasicRequest.RequestType;

public class RegisterRequest extends BasicRequest{
	private static final String TAG = "registerrequest";

	protected String firstname;
	protected String lastname; 
	
	  public RegisterRequest (String firstname, String lastname, String username, String password)
	  {
	      super(username, password);
	      this.firstname = firstname; 
	      this.lastname = lastname;
	  }
	  
	 
	  String host = "http://54.69.96.208:5000/v1";
	  
	  @Override
	  public String getURL() {
	      return "/register";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      List<NameValuePair> params = new ArrayList<NameValuePair>();
	      params.add(new BasicNameValuePair("firstname", this.firstname));
	      params.add(new BasicNameValuePair("lastname", this.lastname));
	      params.add(new BasicNameValuePair("username", this.username));
	      params.add(new BasicNameValuePair("password", this.password));
	      return params;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.POST;
	  }

	  @Override
	  public RegisterResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.v(TAG, "Connecting");
	          if (response.getString("status").equals("success"))
	          {
	              //int playerID = response.getInt("playerid");
	              return new RegisterResponse("success", "registered successfully");
	          } else {
	              
	              String errorMessage = response.getString("status");
	              return new RegisterResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	          return new RegisterResponse("failure", "register not working");
	      } catch (WherewolfNetworkException ex)
	      {
	          return new RegisterResponse("failure", "could not communicate with the server");
	      }
	      
	      
	      
	  }

	}