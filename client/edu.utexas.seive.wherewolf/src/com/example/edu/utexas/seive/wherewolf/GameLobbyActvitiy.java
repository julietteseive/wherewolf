package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;

import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class GameLobbyActvitiy extends Activity {
	private static final String TAG = "loginactivity";
	
	private int startMainScreeActivity()
	{
		Log.v(TAG, "User pressed the join game button");
		Intent intent = new Intent(this, MainScreeActivity.class);
		startActivity(intent);
		return 8;
	}

	private void populatePlayer(){
		ArrayList<Player> arrayOfPlayers = Player.getPlayer();
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_lobby_actvitiy);
		//populatePlayer();
		WherewolfPreferences pref = new WherewolfPreferences(GameLobbyActvitiy.this);
        String username = pref.getUsername();
        
		ArrayList<Player> allPlayers = new ArrayList<Player>();
		PlayerAdapter adapter = new PlayerAdapter(this, allPlayers);
		adapter.add(new Player(1, username, "villager3", 0));
		Log.v(TAG, "WORK<3");
		ListView playerListView = (ListView) findViewById(R.id.players_list);
		Log.v(TAG, "WORK!");
		playerListView.setAdapter(adapter);
		Log.v(TAG, "WORKjgfgh");
		
		final Button button = (Button) findViewById(R.id.startgameButton);

		View.OnClickListener create = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				startMainScreeActivity();
			}
		};
		button.setOnClickListener(create);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_lobby_actvitiy, menu);
		return true;
}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
