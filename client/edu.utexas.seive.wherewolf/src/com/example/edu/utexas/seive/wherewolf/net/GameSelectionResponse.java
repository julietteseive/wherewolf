package com.example.edu.utexas.seive.wherewolf.net;

public class GameSelectionResponse extends BasicResponse{

	  protected int gameID;
	  
	  public GameSelectionResponse (String status, String message)
	  {
	      super(status, message);
	  }
	  
	  public GameSelectionResponse (String status, String message, int gameID)
	  {
	      super(status, message);  
	      this.gameID = gameID;
	  }

	  public int getGameID() {
	      return gameID;
	  }    
	  
	  
	}
