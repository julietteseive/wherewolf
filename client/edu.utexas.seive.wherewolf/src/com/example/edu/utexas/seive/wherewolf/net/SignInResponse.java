package com.example.edu.utexas.seive.wherewolf.net;

public class SignInResponse extends BasicResponse {
  
  private int playerID = -1;

  public SignInResponse(String status, String errorMessage) {
      super(status, errorMessage);
  }
  
  public SignInResponse(String status, String errorMessage, int playerID) {
      super(status, errorMessage);
      
      this.playerID = playerID;
  }

  
  public int getPlayerID()
  {
      return playerID;
  }
  
}
