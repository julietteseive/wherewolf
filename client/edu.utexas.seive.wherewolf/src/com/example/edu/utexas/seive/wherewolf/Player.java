package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;

public class Player {
	private int PlayerId;
	private String name;
	private String profilePicUrl;
	private int numVotes;

	public Player(int playerId, String name, 
			String profilePicUrl, int numVotes)
	{
		this.PlayerId = playerId;
		this.name = name;
		this.profilePicUrl = profilePicUrl;
		this.numVotes = numVotes;
	}

	public String getPlayerId() {
		return Integer.toString(this.PlayerId);
	}

	public void setPlayerId(int playerId) {
		PlayerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public String getNumVotes() {
		return Integer.toString(this.numVotes);
	}

	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}
	
	public static ArrayList<Player> getPlayer(){
	ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
	arrayOfPlayers.add(new Player(1, "malevillager3", "Tom", 5));
	arrayOfPlayers.add(new Player(2, "malevillager3", "George", 3));
	arrayOfPlayers.add(new Player(3, "malevillager3", "Abigail", 1));
	arrayOfPlayers.add(new Player(4, "malevillager3", "Martha", 0));
	return arrayOfPlayers;
	}
}