package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class GameAdapter extends ArrayAdapter<Game> {
	 public GameAdapter(Context context, ArrayList<Game> games) {
	        super(context, 0, games);
	     }
	 
	 @Override
 public View getView(int position, View convertView, ViewGroup parent) {
    Game game = getItem(position);    
    if (convertView == null) {
       convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_game, parent, false);
    }
    TextView gameNameTV = (TextView) convertView.findViewById(R.id.game_name);
    TextView adminNameTV = (TextView) convertView.findViewById(R.id.game_admin);
    //Button joingame = (Button) convertView.findViewById(R.id.joingameButton);
    
    gameNameTV.setText(game.getGameName());
    adminNameTV.setText(game.getAdminName());
    
    return convertView;
}
}