package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;

import com.example.edu.utexas.seive.wherewolf.net.GetGameRequest;
import com.example.edu.utexas.seive.wherewolf.net.GetGameResponse;
import com.example.edu.utexas.seive.wherewolf.net.SignInRequest;
import com.example.edu.utexas.seive.wherewolf.net.SignInResponse;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfNetworking;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.R.array;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class GameSelectionActivity extends Activity {
	private static final String TAG = "loginactivity";
	ArrayList<Game> arrayOfGames = new ArrayList<Game>();
	ArrayList<Game> emptyGameList = new ArrayList<Game>();
private class getGamesTask extends AsyncTask<Void, Integer, GetGameResponse> {
		
		private static final String TAG = "loginactivity";
        		

	    @Override
	    protected GetGameResponse doInBackground(Void... request) {
	        
	    	WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
	        String username = pref.getUsername();
	        String password = pref.getPassword();
	    	
	        GetGameRequest getgamesRequest = new GetGameRequest(username, password);
	        return getgamesRequest.execute(new WherewolfNetworking());
	        
	    
	    }

	    protected void onPostExecute(GetGameResponse result) {

	        Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
	        
	        final TextView errorText = (TextView) findViewById(R.id.error_text4);
	        
	        if (result.getStatus().equals("success")) {
	            Log.v(TAG, "was successful");
	            arrayOfGames = result.getGameInfo();
	            makeAdapter(arrayOfGames);
	            //for (int i = 0; i < arrayOfGames.size(); i++){
	            //	makeAdapter(arrayOfGames.get(i));
	            //	Log.v(TAG, ""+i);
	            //}
	            
	           
	           
	            //Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
	            //startActivity(intent);
	            //overridePendingTransition(R.anim.slide_in_right,
	            //        R.anim.slide_out_right);
	        } else {
	            // do something with bad password
	            
	            errorText.setText(result.getErrorMessage());
	        }

	    }

	    

	}

	///private void populateGame(){
		///ArrayList<Game> arrayOfGames = Game.getGame();
		///GameAdapter adapter = new GameAdapter(this, arrayOfGames);
		//ListView gameListView = (ListView) findViewById(R.id.game_list);
		//gameListView.setAdapter(adapter);
	//}
	
	private void makeAdapter(ArrayList<Game> games){
		Log.v(TAG, "entered the adapter maker");
		ArrayList<Game> allGames = new ArrayList<Game>();
		GameAdapter adapter = new GameAdapter(GameSelectionActivity.this, allGames);
		ListView gameListView = (ListView) findViewById(R.id.game_list);
		gameListView.setAdapter(adapter);
		Log.v(TAG, "size of games: " + ""+games.size());
		for(int i = 0; i<games.size(); i++){
			adapter.add(games.get(i));
			Log.v(TAG, ""+i);
		}
		Log.v(TAG, "WORK!!!!");
		gameListView.setOnItemClickListener(new OnItemClickListener(){
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Log.v(TAG, "WORK");
		startGameLobbyActivity();
				}
		});
			

	}
	private int startGameLobbyActivity()
	{
		Log.v(TAG, "User pressed the join game button");
		Intent intent = new Intent(this, GameLobbyActvitiy.class);
		startActivity(intent);
		return 8;
	}
	
	private int startCreateGameActivity()
	{
		Log.v(TAG, "User pressed the join game button");
		Intent intent = new Intent(this, CreateGameActivity.class);
		startActivity(intent);
		return 8;
	}
	
	private int startLoginActivity()
	{
		Log.v(TAG, "User pressed the leave game button");
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		return 8;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_selection);
		//populateGame();
		
		//ArrayList<Game> allGames = new ArrayList<Game>();
		//GameAdapter adapter = new GameAdapter(this, allGames);

		//ListView gameListView = (ListView) findViewById(R.id.game_list);
		//gameListView.setAdapter(adapter);
		//adapter.add(new Game(1, "The Hunt", "Riley"));
		//adapter.add(new Game(2, "Moonlight", "Juliette"));
		//adapter.add(new Game(3, "WhichWolf", "Sven"));
		
		//Log.v(TAG, "WORK!!!!");
		//gameListView.setOnItemClickListener(new OnItemClickListener(){
		//public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			//	Log.v(TAG, "WORK");
				//startGameLobbyActivity();
			//}
	//});
		Log.v(TAG, "making final button");
		final Button button = (Button) findViewById(R.id.creategameButton);

		View.OnClickListener create = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				startCreateGameActivity();
			}
		};
		
		Log.v(TAG, "setting on click listener");
		button.setOnClickListener(create);
		Log.v(TAG, "executing async");
		new getGamesTask().execute();
		Log.v(TAG, "Async complete");
	
		
		final Button button2 = (Button) findViewById(R.id.logoutButton);

		View.OnClickListener make = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				startLoginActivity();
			}
		};
		button2.setOnClickListener(make);
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_selection, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
