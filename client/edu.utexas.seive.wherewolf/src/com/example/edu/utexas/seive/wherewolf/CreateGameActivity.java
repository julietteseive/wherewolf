package com.example.edu.utexas.seive.wherewolf;

import com.example.edu.utexas.seive.wherewolf.net.CreateGameRequest;
import com.example.edu.utexas.seive.wherewolf.net.CreateGameResponse;
import com.example.edu.utexas.seive.wherewolf.net.RegisterRequest;
import com.example.edu.utexas.seive.wherewolf.net.RegisterResponse;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfNetworking;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CreateGameActivity extends Activity {
	private static final String TAG = "creategameactivity";
	
	
	private class CreateGameTask extends AsyncTask<Void, Integer, CreateGameResponse> {

	    @Override
	    protected CreateGameResponse doInBackground(Void... request) {
	    	
	        final EditText gamedescriptionTV = (EditText) findViewById(R.id.gamedescriptionText);
	        final EditText gamenameTV = (EditText) findViewById(R.id.gamenameText);
	        
	        String gameDescription = gamedescriptionTV.getText().toString();
	        String gameName = gamenameTV.getText().toString();
	        
	        Log.v("TAG", "should happen..");
	        WherewolfPreferences pref = new WherewolfPreferences(CreateGameActivity.this);
	        String username = pref.getUsername();
	        String password = pref.getPassword();
	        Log.v("TAG", username);
	        Log.v("TAG", password);
	        CreateGameRequest creategameRequest = new CreateGameRequest(username, password, gameName, gameDescription);
	        
	        return creategameRequest.execute(new WherewolfNetworking());
	        
	    
	    }

	    protected void onPostExecute(CreateGameResponse result) {
	        
	        final TextView errorText = (TextView) findViewById(R.id.error_text3);
	        
	        if (result.getStatus().equals("success")) {
	        	Log.v("TAG", "inside if");
	        	
	        	WherewolfPreferences pref = new WherewolfPreferences(CreateGameActivity.this);
	            pref.setCurrentGameID(result.getGameID());
	            		
	            errorText.setText("");
	            Log.v(TAG, "Signing in");
	            startGameLobbyActivity();
	            //Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
	            //startActivity(intent);
	            //overridePendingTransition(R.anim.slide_in_right,
	            //        R.anim.slide_out_right);
	        } else {
	            
	            errorText.setText(result.getErrorMessage());
	        }

	    }

	    

	}
	
	private int startGameLobbyActivity()
	{
		Log.v(TAG, "User pressed the create game button");
		Intent intent = new Intent(this, GameLobbyActvitiy.class);
		startActivity(intent);
		return 8;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_game);
		
		final Button button = (Button) findViewById(R.id.creategame_button);

		View.OnClickListener create = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				new CreateGameTask().execute();
			}
		};
		button.setOnClickListener(create);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
