package com.example.edu.utexas.seive.wherewolf;


import com.example.edu.utexas.seive.wherewolf.net.RegisterRequest;
import com.example.edu.utexas.seive.wherewolf.net.RegisterResponse;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfNetworking;
import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends Activity {

	
	private static final String TAG = "registeractivity";
	
private class RegisterTask extends AsyncTask<Void, Integer, RegisterResponse> {

	    @Override
	    protected RegisterResponse doInBackground(Void... request) {
	    	
	        final EditText nameTV = (EditText) findViewById(R.id.usernameText);
	        final EditText passTV = (EditText) findViewById(R.id.EditText01);
	        
	        String firstname = " ";
	        String lastname = " ";
	        String username = nameTV.getText().toString();
	        String password = passTV.getText().toString();
	        
	        RegisterRequest registerRequest = new RegisterRequest(firstname, lastname, username, password);
	        
	        return registerRequest.execute(new WherewolfNetworking());
	        
	    
	    }

	    protected void onPostExecute(RegisterResponse result) {

	        Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
	        
	        final TextView errorText = (TextView) findViewById(R.id.error_text2);
	        
	        if (result.getStatus().equals("success")) {
	        	Log.v("TAG", "inside if");
	            errorText.setText("");
	            Log.v(TAG, "Signing in");
	            startLoginActivity();
	            //Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
	            //startActivity(intent);
	            //overridePendingTransition(R.anim.slide_in_right,
	            //        R.anim.slide_out_right);
	        } else {
	            // do something with bad password
	            
	            errorText.setText(result.getErrorMessage());
	        }

	    }

	    

	}
	
	
	public void stopRegistering()
	{
		Log.v(TAG, "closing the register screen");
		this.finish();
	}
	
	private int startLoginActivity()
	{
		Log.v(TAG, "User pressed the register button");
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		return 8;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
	
		final Button button = (Button) findViewById(R.id.register_user_button);

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				new RegisterTask().execute();
			}
		};
		
		button.setOnClickListener(jim);
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
