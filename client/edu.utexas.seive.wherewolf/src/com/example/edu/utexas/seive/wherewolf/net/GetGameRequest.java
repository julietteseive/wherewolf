package com.example.edu.utexas.seive.wherewolf.net;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.example.edu.utexas.seive.wherewolf.net.BasicRequest.RequestType;

public class GetGameRequest extends BasicRequest {
	private static final String TAG = "getgamerequest";

	  public GetGameRequest (String username, String password)
	  {
	      super(username, password);
	  }
	  
	 
	  String host = "http://54.69.96.208:5000/v1";
	  
	  @Override
	  public String getURL() {
	      return "/game";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      return null;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.GET;
	  }

	  @Override
	  public GetGameResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.v(TAG, "Connecting");
	          Log.v(TAG, ""+response);
	          if (response.getString("status").equals("success"))
	          {
	              //int playerID = response.getInt("playerid");
	        	  Log.v(TAG, ""+(response.getJSONArray("results")));
	              return new GetGameResponse("success", "it worked", response.getJSONArray("results"));
	          } else {
	              
	              String errorMessage = response.getString("status");
	              return new GetGameResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	          return new GetGameResponse("failure", "adding games not working");
	      } catch (WherewolfNetworkException ex)
	      {
	          return new GetGameResponse("failure", "could not communicate with the server");
	      }
	      
	      
	      
	  }

	}
