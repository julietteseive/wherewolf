package com.example.edu.utexas.seive.wherewolf;

import java.util.ArrayList;
import java.util.Calendar;

import com.example.edu.utexas.seive.wherewolf.net.WherewolfPreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.AdapterView.OnItemClickListener;


public class MainScreeActivity extends Activity {
	private static final String TAG = "mainscreenactivity";
	int currentTime;
	CircadianWidgetView circadianWidget;
	ArrayList<Player> allPlayers;
	PlayerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_scree);
		
		circadianWidget = (CircadianWidgetView) findViewById(R.id.circadian);
        final SeekBar seekbar = (SeekBar) findViewById(R.id.daytime_seekbar);
		
		
		//get current time
		Calendar cal = Calendar.getInstance();
		currentTime = (cal.get(Calendar.HOUR_OF_DAY)*100) + cal.get(Calendar.MINUTE);
		Log.i(TAG, "current time is "+currentTime);
		seekbar.setMax(2399);
		seekbar.setProgress(currentTime);
		
		//set on change listener for seek bar
		seekbar.setOnSeekBarChangeListener(mySeekBarChangeListener());
		
		WherewolfPreferences pref = new WherewolfPreferences(MainScreeActivity.this);
        String username = pref.getUsername();

		// Create the adapter to convert the array to views
		allPlayers = new ArrayList<Player>();
		adapter = new PlayerAdapter(this, allPlayers);
		
		
		final PlayerAdapter adapter = new PlayerAdapter(this, allPlayers);
		ListView playerListView = (ListView) findViewById(R.id.players_list2);
		playerListView.setAdapter(adapter);
		adapter.add(new Player(1, username, "villager3", 0));
		
//		allPlayers.add(new Player(1, "Tom", "Tom", 0));
//		allPlayers.add(new Player(2, "George", "George", 0));
//		allPlayers.add(new Player(3, "Henry", "Abigail", 0));
//		allPlayers.add(new Player(4, "Peter", "Martha", 0));
		//populate array with fake data
		
		

		playerListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// add attack and vote capabilities
				
                Player p = adapter.getItem((int) id);
                Log.i(TAG, "clicked on player " + p.getName());
                if (isNight()==1){ 
                	Log.i(TAG, "It's night");
                	// add werewolf attack
                	
                } else {
                	Log.i(TAG, "It's day");
                	// add voting
                	int votes = Integer.parseInt(p.getNumVotes());
                    p.setNumVotes(votes+1);
                	adapter.notifyDataSetChanged();
                    Log.i(TAG, "player " + p.getName() + " has " + p.getNumVotes()+" votes");
                }
			}
		});
		
		Log.v(TAG, "making another button");
		final Button button = (Button) findViewById(R.id.leaveButton);
		
		View.OnClickListener joe = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTTTTTTTT");
				startGameSelectionActivity();
			}
		};
		button.setOnClickListener(joe);
	}

	private OnSeekBarChangeListener mySeekBarChangeListener(){
		return new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
				//get time to change circadian widget
				double time = (double) progress/100;
				currentTime = progress;
				Log.i(TAG, "current time is "+time);
				circadianWidget.changeTime(time);
				}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub	
				}
			};
		}
	
	public void addPlayer(int id, String img, String name, int numVotes) {
	    Log.i(TAG, ""+numVotes);
		adapter.add(new Player(id, img, name, numVotes));
		adapter.notifyDataSetChanged();
	}
	
	private int startGameSelectionActivity()
	{
		Log.v(TAG, "User pressed the leave game button");
		Intent intent = new Intent(this, GameSelectionActivity.class);
		startActivity(intent);
		return 8;
	}
	
	public int isNight(){
		return (currentTime >= 600 && currentTime< 1800)? 0:1;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_scree, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
