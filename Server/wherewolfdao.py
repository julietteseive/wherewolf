# Wherewolf game DAO
# Abstraction for the SQL database access.

import psycopg2
import md5
import datetime
import random
import math


class UserAlreadyExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class NoUserExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class BadArgumentsException(Exception):
    """Exception for entering bad arguments"""
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err

class WherewolfDao:

    def __init__(self, dbname='wherewolf', pgusername='juliette', pgpasswd='eiffel78'):
        self.dbname = dbname
        self.pgusername = pgusername
        self.pgpasswd = pgpasswd

    def get_db(self):
        return psycopg2.connect(host = 'wherewolf.c3jcb3gxwsa7.us-west-2.rds.amazonaws.com', database=self.dbname,user=self.pgusername,password=self.pgpasswd)

    def create_user(self, username, password, firstname, lastname):
        """ registers a new player in the system """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT COUNT(*) from gameuser WHERE username=%s',(username,))
            n = int(c.fetchone()[0])
            # print 'num of rfdickersons is ' + str(n)
            if n == 0:
                hashedpass = md5.new(password).hexdigest()
                c.execute('INSERT INTO gameuser (username, password, firstname, lastname) VALUES (%s,%s,%s,%s)', 
                          (username, hashedpass, firstname, lastname))
                conn.commit()
            else:
                raise UserAlreadyExistsException('{} user already exists'.format((username)) )
        
    def check_password(self, username, password):
        """ return true if password checks out """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            sql = ('select password from gameuser where username=%s')
            c.execute(sql,(username,))
            hashedpass = md5.new(password).hexdigest()
            u = c.fetchone()
            if u == None:
                raise NoUserExistsException(username)
            # print 'database contains {}, entered password was {}'.format(u[0],hashedpass)
            return u[0] == hashedpass
        
    def set_location(self, username, lat, lng):
        conn = self.get_db()
	with conn:
	    cur = conn.cursor()
            sql = ('update player set lat=%s, lng=%s '
                   'where player_id=(select current_player from gameuser '
                   'where username=%s)')
	    cur.execute(sql, (lat, lng, username))
            conn.commit()


    def get_location(self, username):
        conn = self.get_db()
	result = {}
        with conn:
	    c = conn.cursor()
            sql = ('select player_id, lat, lng from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s')
	    c.execute(sql, (username,))
	    row = c.fetchone()
            result["playerid"] = row[0]
            result["lat"] = row[1]
            result["lng"] = row[2]
        return result

    def get_werewolf_nearby(self, username, game_id, radius): 
        ''' returns all alive players near a player '''
        conn = self.get_db()
        result = []
        with conn:
            c = conn.cursor()
            sql_location = ('select lat, lng from player, gameuser where '
                           'player.player_id = gameuser.current_player '
                           'and gameuser.username=%s')
            c.execute(sql_location, (username,))
            location = c.fetchone()

            if location == None:
                return result

            # using the radius for lookups now
            sql = ('select player_id, '
                   'earth_distance( ll_to_earth(player.lat, player.lng), '
                   'll_to_earth(%s,%s) ) '
                   'from player where '
                   'earth_box(ll_to_earth(%s,%s),%s) '
                   '@> ll_to_earth(player.lat, player.lng) '
                   'and game_id=%s '
                   'and is_werewolf > 0 '
                   'and is_dead = 0')

            c.execute(sql, (location[0], location[1], 
                            location[0], location[1], 
                            radius, game_id))
            for row in c.fetchall():
                d = {}
                d["player_id"] = row[0]
                d["distance"] = row[1]
                result.append(d)
        return result        

    def get_alive_nearby(self, username, game_id, radius): 
        ''' returns all alive players near a player '''
        conn = self.get_db()
        result = []
        with conn:
            c = conn.cursor()
            sql_location = ('select lat, lng from player, gameuser where '
                           'player.player_id = gameuser.current_player '
                           'and gameuser.username=%s')
            c.execute(sql_location, (username,))
            location = c.fetchone()

            if location == None:
                return result

            current_player = self.get_user_info(username)[2]
            c.execute('SELECT lat, lng FROM player WHERE player_id=%s', (current_player,))
            loc = c.fetchone()
            ulat = loc[0]
            ulng = loc[1]

            # using the radius for lookups now
            """sql = ('select player_id, '
                   'earth_distance( ll_to_earth(player.lat, player.lng), '
                   'll_to_earth(%s,%s) ) '
                   'from player where '
                   'earth_box(ll_to_earth(%s,%s),%s) '
                   '@> ll_to_earth(player.lat, player.lng) '
                   'and game_id=%s '
                   'and is_werewolf = 0 '
                   'and is_dead = 0')"""

            c.execute('SELECT player_id, ((%s - lat)*(%s-lat) + (%s - lng)*(%s - lng)) FROM player WHERE ((%s - lat)*(%s-lat) + (%s - lng)*(%s - lng))<%s and game_id=%s', (ulat, ulat, ulng, ulng, ulat, ulat, ulng, ulng, radius*radius, game_id))
            nearby = c.fetchall()
            

            """c.execute(sql, (location[0], location[1], 
                            location[0], location[1], 
                            radius, game_id))"""
            for row in nearby:
                d = {}
                d["player_id"] = row[0]
                d["distance"] = math.sqrt(float(row[1]))
                result.append(d)
        return result
                   
        
    def add_item(self, username, itemname):
        conn = self.get_db()
	with conn:
	    cur=conn.cursor()

            cmdupdate = ('update inventory set quantity=quantity+1'
                         'where itemid=(select itemid from item where name=%s)' 
                         'and playerid='
                         '(select current_player from gameuser where username=%s);')
            cmd = ('insert into inventory (playerid, itemid, quantity)' 
                   'select (select current_player from gameuser where username=%s) as cplayer,'
                   '(select itemid from item where name=%s) as item,' 
                   '1 where not exists' 
                   '(select 1 from inventory where itemid=(select itemid from item where name=%s)' 
                   'and playerid=(select current_player from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (itemname, username, username, itemname, itemname, username))

	    conn.commit()

 
    def remove_item(self, username, itemname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('update inventory set quantity=quantity-1 where ' 
                   'itemid=(select itemid from item where name=%s) and ' 
                   'playerid=(select current_player from gameuser where username=%s);')
            cmddelete = ('delete from inventory where itemid=(select itemid from item where name=%s)' 
                         'and playerid=(select current_player from gameuser where username=%s) '
                         'and quantity < 1;')
            cur.execute(cmd + cmddelete, (itemname, username, itemname, username))
            conn.commit()


    def get_items(self, username):
        conn = self.get_db()
	items = []
        with conn:
	    c = conn.cursor()
            sql = ('select item.name, item.description, quantity, item.w_damage, item.a_rating '
                   'from item, inventory, gameuser where '
                   'inventory.itemid = item.itemid and '
                   'gameuser.current_player=inventory.playerid and '
                   'gameuser.username=%s')
            c.execute(sql, (username,))
            for item in c.fetchall():
                d = {}
                d["name"] = item[0]
                d["description"] = item[1]
                d["quantity"] = item[2]
                d["w_damage"] = item[3]
                d["a_rating"] = item[4]
                items.append(d)
        return items

        
    def award_achievement(self, username, achievementname):
        conn = self.get_db()
        with conn:
	    cur=conn.cursor()
            cmd = ('insert into user_achievement (user_id, achievement_id, created_at) '
                   'values ((select user_id from gameuser where username=%s), '
                   '(select achievement_id from achievement where name=%s), now());')
            cur.execute(cmd, (username, achievementname))
            conn.commit()

        
    def get_achievements(self, username):
        conn = self.get_db()
	with conn:
            cur = conn.cursor()
            cmd = ('select name, description, created_at from achievement, user_achievement '
                   'where achievement.achievement_id = user_achievement.achievement_id '
                   'and user_achievement.user_id = '
                   '(select user_id from gameuser where username=%s);')
            cur.execute(cmd, (username,))
	    achievements = []
	    for row in cur.fetchall():
                d = {}
                d["name"] = row[0]
                d["description"] = row[1]
                d["created_at"] = row[2]
                achievements.append(d)
	return achievements

    def set_dead(self, username):
        conn = self.get_db()
	with conn:
	    cur = conn.cursor()
            cmd = ('update player set is_dead=1 '
                   'where player_id='
                   '(select current_player from gameuser where username=%s);')
            cur.execute(cmd, (username,))
            conn.commit()

    def get_players(self, gameid):
        conn = self.get_db()
        players = []
        with conn:
            cur = conn.cursor()
            cmd = ('select player_id, is_dead, lat, lng, is_werewolf from player '
                   ' where game_id=%s;')
            cur.execute(cmd, (gameid,))
            for row in cur.fetchall():
                p = {}
                p["playerid"] = row[0]
                p["is_dead"] = row[1]
                p["lat"] = row[2]
                p["lng"] = row[3]
                p["is_werewolf"] = row[4]
                players.append(p)
        return players

    def get_user_stats(self, username):
        pass
	    
    def set_player_stat(self, username, stat_name, stat_value):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT current_player FROM gameuser WHERE username=%s', (username,))
            player_id = c.fetchone()[0]
            if self.get_player_stat(username, stat_name) is None:
                c.execute('INSERT INTO player_stat (player_id, stat_name, stat_value) VALUES (%s, %s, %s)', (player_id, stat_name, stat_value))
            else:
                c.execute('UPDATE player_stat SET stat_value=%s, quantity = quantity + 1 WHERE player_id=%s AND stat_name=%s', (stat_value, player_id, stat_name))
            conn.commit()

    def get_player_stat(self, username, stat_name):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT current_player FROM gameuser WHERE username=%s', (username,))
            playerid = c.fetchone()[0]
            c.execute('SELECT * FROM player_stat WHERE player_id=%s AND stat_name=%s', (playerid, stat_name))
            player_stats = c.fetchone()
            conn.commit()
        if player_stats == None:
            return player_stats
        return player_stats

    # game methods    
    def join_game(self, username, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('INSERT INTO player ( is_dead, lat, lng, game_id) '
                   'VALUES ( %s, %s, %s, %s) returning player_id')
            cmd2 = ('update gameuser set current_player=%s where username=%s')
            cur.execute(cmd,( 0, 0, 0, gameid))
            cur.execute(cmd2, (cur.fetchone()[0], username));
            conn.commit()
	
    def leave_game(self, username):
        conn = self.get_db()
	with conn:
            cur = conn.cursor()
            cmd1 = '''UPDATE gameuser set current_player = -1 where username=%s'''
            cur.execute(cmd1, (username,)) 
            conn.commit()
	    
        
    def create_game(self, username, gamename, description, radius, lat, lng):
        ''' returns the game id for that game '''
        conn = self.get_db()
	with conn:
	    cur = conn.cursor()
	    cmd = ('INSERT INTO game (admin_id, name, description, game_radius, game_lat, game_lng) VALUES ( '
                   '(SELECT user_id FROM gameuser where username=%s), '
                   '%s, %s, %s, %s, %s) returning game_id')
            cur.execute(cmd,(username, gamename, description, radius, lat, lng))
            game_id = cur.fetchone()[0]
            conn.commit()
            self.join_game(username, game_id)
        return game_id


    def game_info(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT game_id, admin_id, status, name, game_radius, game_lat, game_lng from game where game_id=%s'''
            cur.execute(cmd, (game_id,))
            row = cur.fetchone()
            d = {}
            d["game_id"] = row[0]
            d["admin_id"] = row[1]
            d["status"] = row[2]
            d["name"] = row[3]
            d["game_radius"] = row[4]
            d["game_lat"] = row[5]
            d["game_lng"] = row[6]
            conn.commit()
        return d

    def get_games(self):
        conn = self.get_db()
        games = []
        with conn:
            cur = conn.cursor()
            cmd = ('SELECT game_id, name, status from game')
            cur.execute(cmd)
            for row in cur.fetchall():
                d = {}
                d["game_id"] = row[0]
                d["name"] = row[1]
                d["status"] = row[2]
                games.append(d)
            conn.commit()
        return games

            
    def set_game_status(self, game_id, status):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('UPDATE game set status=%s '
                   'where game_id=%s')
            cur.execute(cmd, (status, game_id))
            conn.commit()
        

    def vote(self, game_id, player_id, target_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into vote '
                   '(game_id, player_id, target_id, cast_date) '
                   'values ( %s,'
                   '(select current_player from gameuser where username=%s), '
                   '(select current_player from gameuser where username=%s), '
                   'now())')
            cur.execute(sql, (game_id, player_id, target_id))
            conn.commit()

    def get_votes(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cur.execute('SELECT target_id, player_id, is_current FROM vote WHERE game_id=%s', (game_id,))
            votes = cur.fetchall()
            conn.commit()
        return votes
    
    def clear_tables(self):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('truncate vote cascade')
            c.execute('truncate player_stat cascade')
            c.execute('truncate user_stat cascade')
            c.execute('truncate treasure cascade')
            c.execute('truncate inventory cascade')
            c.execute('truncate landmark cascade')
            c.execute('truncate player cascade')
            c.execute('truncate game cascade')
            c.execute('truncate gameuser cascade')
            c.execute('truncate user_achievement cascade')
            c.execute('truncate user_stat cascade')
            conn.commit()

    def get_user_info(self, username):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT user_id, password, current_player FROM gameuser WHERE username=%s', (username,))
            userinfo = c.fetchone()
            conn.commit()
        return userinfo

    def get_player_info(self, player_id):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT is_dead, lat, lng, is_werewolf, num_gold, game_id FROM player WHERE player_id=%s', (player_id,))
            row = c.fetchone()
            d = {}
            d["is_dead"] = row[0]
            d["lat"] = row[1]
            d["lng"] = row[2]
            d["is_werewolf"] = row[3]
            d["num_gold"] = row[4]
            d["game_id"] = row[5]
            conn.commit()
        return d

    def get_game_users(self, gameid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            players = self.get_players(gameid)
            users = []
            for player in players:
                playerid = player["playerid"]
                c.execute('SELECT username FROM gameuser WHERE current_player=%s', (playerid,))
                username = c.fetchone()
                if username is not None:
                    username = username[0]
                    users.append(username)
            conn.commit()
            return users

    def set_time(self, timehour, timeminute, gameid):
        conn = self.get_db()
        with conn:
            if timehour == "current":
                time = "-1"
            else:
                hour = timehour
                minute = timeminute
            	time = str(hour) + ',' + str(minute)
            c = conn.cursor()
            c.execute('UPDATE game SET game_time=%s WHERE game_id=%s', (time, gameid))
            conn.commit()

    def get_time(self, gameid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT game_time FROM game WHERE game_id=%s', (gameid,))
            time = c.fetchone()
            if time[0] == "-1":
                hour = datetime.datetime.now().hour
                minute = datetime.datetime.now().minute
                time = str(hour) + ',' + str(minute)
            conn.commit()
        return time[0]

    def level_up(self, username):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT current_player FROM gameuser WHERE username=%s', (username,))
            playerid = c.fetchone()[0]
            c.execute('SELECT is_werewolf FROM player WHERE player_id=%s', (playerid,))
            level = c.fetchone()[0]
            if level < 3:
                c.execute('UPDATE player SET is_werewolf = is_werewolf + 1 WHERE player_id=%s', (playerid,))
            conn.commit()

    def set_werewolf(self, playerid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('UPDATE player SET is_werewolf=1 WHERE player_id=%s', (playerid,))
            conn.commit()
    
    def get_user(self, playerid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT username FROM gameuser WHERE current_player=%s', (playerid,))
            username = c.fetchone()[0]
            conn.commit()
        return username

    def create_landmark(self, game_id, lat, lng, radius, type1):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('INSERT INTO landmark (game_id, lat, lng, radius, type, is_active) VALUES(%s, %s, %s, %s, %s, %s) RETURNING landmark_id', (game_id, lat, lng, radius, type1, 1))
            landmark_id = c.fetchone()[0]
            if type1 == 'treasure':
                c.execute('SELECT itemid FROM item')
                items = c.fetchall()
                random.shuffle(items)
                c.execute('INSERT INTO treasure (landmark_id, item_id) VALUES (%s, %s)', (landmark_id, items[0][0]))
            conn.commit()

    def get_landmarks(self, game_id):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT lat, lng, radius, type, landmark_id FROM landmark WHERE game_id=%s AND is_active=%s', (game_id, 1))
            result = []
            for row in c.fetchall():
                d = {}
                d["lat"] = row[0]
                d["lng"] = row[1]
                d["radius"] = row[2]
                d["type"] = row[3]
                d["landmark_id"] = row[4]
                result.append(d)
            conn.commit()
        return result
     
    def get_treasure_taken(self, username):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT current_player FROM gameuser WHERE username=%s', (username,))
            playerid = c.fetchone()[0]
            c.execute('SELECT landmark_id FROM treasure_taken WHERE playerid=%s', (playerid,))
            temp = c.fetchall()
            landmarks = []
            for row in temp:
                landmarks.append(row[0])
            conn.commit()
        return landmarks

    def get_treasure_item(self, landmarkid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT item_id FROM treasure WHERE landmark_id=%s', (landmarkid,))
            temp = c.fetchall()
            items = []
            for row in temp:
                items.append(row[0])
            conn.commit()
        return items

    def get_item_info(self, itemid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT itemid, name, description FROM item WHERE itemid=%s', (itemid,))
            row = c.fetchone()
            d = {}
            d["itemid"] = row[0]
            d["name"] = row[1]
            d["description"] = row[2]
            conn.commit()
        return d

    def disactivate_landmark(self, landmarkid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('UPDATE landmark SET is_active=%s WHERE landmark_id=%s', (0, landmarkid))
            conn.commit()
        return
   
    def clear_votes(self, gameid):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('UPDATE vote SET is_current=%s WHERE game_id=%s', (0, gameid))
            conn.commit()   
            
if __name__ == "__main__":
    """dao = WherewolfDao('wherewolf')

    dao.clear_tables()
    try:
        dao.create_user('rfdickerson', 'awesome', 'Robert', 'Dickerson')
        dao.create_user('oliver','furry','Oliver','Cat')
        dao.create_user('vanhelsing', 'van', 'Van', 'Helsing')
        print 'Created a new player!'
    except UserAlreadyExistsException as e:
        print e
    except Exception:
        print 'General error happened'
        
    username = 'rfdickerson'
    correct_pass = 'awesome'
    incorrect_pass = 'scaley'
    print 'Logging in {} with {}'.format(username, correct_pass)
    print 'Result: {} '.format( dao.check_password(username, correct_pass ))
    
    print 'Logging in {} with {}'.format(username, incorrect_pass)
    print 'Result: {} '.format( dao.check_password(username, incorrect_pass ))

    game_id = dao.create_game('rfdickerson', 'TheGame')
    # dao.create_game('oliver', 'AnotherGame')
    
    dao.join_game('oliver', game_id)
    dao.join_game('rfdickerson', game_id)
    dao.join_game('vanhelsing', game_id)

    print "Adding some items..."
    dao.add_item('rfdickerson', 'Silver Knife')
    dao.add_item('rfdickerson', 'Blunderbuss')
    dao.add_item('rfdickerson', 'Blunderbuss')
    dao.add_item('rfdickerson', 'Blunderbuss')
    dao.add_item('oliver', 'Blunderbuss')
    dao.remove_item('rfdickerson', 'Blunderbuss')

    print
    print 'rfdickerson items'
    print '--------------------------------'
    items = dao.get_items("rfdickerson")
    for item in items:
        print item["name"] + "\t" + str(item["quantity"])
    print

    # location stuff
    dao.set_location('rfdickerson', 30.25, 97.75)
    dao.set_location('oliver', 30.3, 97.76)
    dao.set_location('vanhelsing', 30.2, 97.7) 
    loc = dao.get_location('rfdickerson')
    loc2 = dao.get_location('oliver')
    print "rfdickerson at {}, {}".format(loc["lat"], loc["lng"]) 
    print "oliver at {}, {}".format(loc2["lat"], loc2["lng"]) 

    dao.award_achievement('rfdickerson', 'Children of the moon')
    dao.award_achievement('rfdickerson', 'A hairy situation')
    achievements = dao.get_achievements("rfdickerson")

    print
    print 'rfdickerson\'s achievements'
    print '--------------------------------'
    for a in achievements:
        print "{} ({}) - {}".format(a["name"],a["description"],a["created_at"].strftime('%a, %H:%M'))
    print
    
    nearby = dao.get_alive_nearby('rfdickerson', game_id, 20)
    for p in nearby:
        print "{} a {} is {} miles away".format(p["player_id"], p["is_werewolf"], p["distance"])

    dao.vote(game_id, 'rfdickerson', 'oliver')
    dao.vote(game_id, 'oliver', 'vanhelsing')
    dao.vote(game_id, 'vanhelsing', 'oliver')
    # print 'Players in game 1 are'
    # print dao.get_players(1)
    
    dao.set_dead('rfdickerson')"""
