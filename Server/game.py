from flask import Flask, request, jsonify
from wherewolfdao import WherewolfDao, UserAlreadyExistsException, NoUserExistsException, BadArgumentsException
import md5
import datetime
import random
import math

DAYBREAKHOUR = 7
NIGHTFALLHOUR = 20
WWHP = [10, 12, 14]
WWATTACK = [3, 5, 7]
VHP = 10
VATTACK = 1
ARMOR = {'unarmored': 0, 'light': 2, 'medium': 4, 'heavy': 6}
WEAPON = {'unarmed': 0, 'light': 2, 'medium': 4, 'heavy': 6}
NEARBY_RADIUS = 500
GAME_RADIUS = 1000
GAME_LAT = 0
GAME_LNG = 0
LMK_RADIUS = 50


app = Flask(__name__)

@app.route("/healthcheck")
def health_check():
    return "healthy"

@app.route("/v1/test/playerid", methods=["POST"])
def testplayerid():
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.form["username"]
    playerid = myDao.get_user_info(username)[2]
    response = {}
    response["status"] = "success"
    response["results"] = playerid
    return jsonify(response)


@app.route("/v1/game/<game_id>/getlandmarks", methods=["GET"])
def get_landmarks(game_id):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    landmarks = myDao.get_landmarks(game_id)
    response["results"] = landmarks
    return jsonify(response)

@app.route("/v1/game/<game_id>/getgameusers", methods=["GET"])
def get_game_users(game_id):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    gameusers = myDao.get_game_users(game_id)
    response["results"] = gameusers
    return jsonify(response)

@app.route("/v1/game/<playerid>/getplayerinfo", methods=["GET"])
def get_player_info(playerid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    playerinfo = myDao.get_player_info(playerid)
    response["results"] = playerinfo
    return jsonify(response)

@app.route("/v1/game/<playerid>/getuser")
def get_user(playerid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    user = myDao.get_user(playerid)
    response["results"] = user
    return jsonify(response)

@app.route("/v1/<username>/getachievements", methods=["GET"])
def get_achievements(username):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    achievements = myDao.get_achievements(username)
    response["results"] = achievements
    return jsonify(response)

@app.route("/v1/<username>/getitems", methods=["GET"])
def get_items(username):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    items = myDao.get_items(username)
    response["results"] = items
    return jsonify(response)
    

@app.route("/v1/game/<gameid>/getplayers", methods=["GET"])
def get_players(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"
    players = myDao.get_players(gameid)
    response["results"] = players
    return jsonify(response)

@app.route("/v1/game/<gameid>/getnearby", methods=["GET"])
def get_alive_nearby(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization["username"]
    response = {}
    response["status"] = "success"
    nearby = myDao.get_alive_nearby(username, gameid, NEARBY_RADIUS)
    response["results"] = nearby
    return jsonify(response)

@app.route("/v1/game/<gameid>/getvotes", methods=["GET"])
def get_current_votes(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    votes = myDao.get_votes(gameid)
    response = {}
    response["status"] = "success"
    response["results"] = votes
    return jsonify(response)

def get_dao(databasename='wherewolf', username='juliette', password='eiffel78'):
    myDao = WherewolfDao(databasename, username, password)
    return myDao

def daybreak(gameid):
    print "THE DAY HATH BROKEN"
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    
    #clear old landmarks
    landmarks = myDao.get_landmarks(gameid)
    for landmark in landmarks:
        myDao.disactivate_landmark(landmark["landmark_id"])
    
    #clear old votes
    myDao.clear_votes(gameid)

def nightfall(gameid):
        print "NIGHT HATH FALLEN"
        myDao = get_dao('wherewolf', 'juliette', 'eiffel78')

        #count votes
        votes = myDao.get_votes(gameid)
        voteList = []
        tally = []
        for item in votes:
            if item[2] == 1:
                voteList.append(item[0])
        playerids = set(voteList)
        count = 0
        for playerid in playerids:
            tally.append({})
            tally[count]['playerid'] = playerid
            tally[count]['votes'] = 0
            for vote in votes:
                if vote[0] == tally[count]['playerid']:
                    tally[count]['votes'] += 1
            count += 1
        numVotes = []
        for thing in tally:
            numVotes.append(thing["votes"])
        maximum = max(numVotes)
        convict = 'temprorary'
        for thing in tally:
            if thing["votes"] == maximum:
                convict = thing["playerid"]

        #imprison the player with highest votes
        username = myDao.get_user(convict)
        myDao.set_dead(username)
        print "{} was imprisoned for recieveing the highest number of votes".format(username)

        #give the lupus stat to people who voted against a villager
        playerinfo = myDao.get_player_info(convict)
        if playerinfo["is_werewolf"] == 0:
            lupi = []
            for vote in votes:
                if vote[0] == convict:
                    lupi.append(vote[1])
            for loop in lupi:
                username = myDao.get_user(loop)
                myDao.set_player_stat(username, 'lupus', 1)
        
        #check if all werewolves are dead, if so end game
        players = myDao.get_players(gameid)
        stop_playing = True
        for player in players:
            if player["is_werewolf"] > 0 and player["is_dead"] == 0:
                stop_playing = False
        if stop_playing:
            end_game(gameid)

        #generate landmarks at random location
        angle = math.radians(random.randint(0, 360))
        dist = random.random()*(GAME_RADIUS - LMK_RADIUS)
        lat = GAME_LAT + math.sin(angle)*dist
        lng = GAME_LNG + math.cos(angle)*dist
        myDao.create_landmark(gameid, lat, lng, LMK_RADIUS, 'safezone')

        angle = math.radians(random.randint(0, 360))
        dist = random.random()*(GAME_RADIUS - LMK_RADIUS)
        lat = GAME_LAT + math.sin(angle)*dist
        lng = GAME_LNG + math.cos(angle)*dist
        myDao.create_landmark(gameid, lat, lng, LMK_RADIUS, 'treasure')

def end_game(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    players = myDao.get_players(gameid)

    #AWARD ACHIEMENTS
    #Leader of the pack
    kills = []
    users = []
    for player in players:
        if player["is_werewolf"] > 0:
            username = myDao.get_user(player["playerid"])
            numKills = myDao.get_player_stat(username, 'kill')
            if numKills is not None:
                numKills = numKills[3]
                users.append(username)
                kills.append(numKills)
    if len(kills) != 0:
        maximum = max(kills)
        index = 0
        count = 0
        for value in kills:
            if value == maximum:
                index = count
            count += 1
        leader_username = users[index]

        achievements = myDao.get_achievements(leader_username)
        has_achievement = False
        for achievement in achievements:
            if achievement["name"] == "Leader of the pack":
                has_achievement = True
        if not has_achievement:
            myDao.award_achievement(leader_username, "Leader of the pack")
            print "Awarded Leader of the pack to {}".format(username)

    #Hair of the dog
    for player in players:
        if player["is_werewolf"] == 0:
            username = myDao.get_user(player["playerid"])
            if myDao.get_player_stat(username, 'survival') is not None:
                achievements = myDao.get_achievements(username)
                has_achievement = False
                for achievement in achievements:
                    if achievement["name"] == "Hair of the dog":
                        has_achievement = True
                if not has_achievement:
                    myDao.award_achievement(username, "Hair of the dog")
                    print "Awarded Hair of the dog to {}".format(username)
    
    #It is never Lupus
    for player in players:
        username = myDao.get_user(player["playerid"])
        if myDao.get_player_stat(username, 'lupus') is not None:
            achievements = myDao.get_achievements(username)
            has_achievement = False
            for achievement in achievements:
                if achievement["name"] == "It is never Lupus":
                    has_achievement = True
            if not has_achievement:
                myDao.award_achievement(username, "It is never Lupus")
                print "Awarded It is never Lupus to {}".format(username)

    #A hairy situation
    for player in players:
        username = myDao.get_user(player["playerid"])
        if myDao.get_player_stat(username, 'hairy') is not None:
            achievements = myDao.get_achievements(username)
            has_achievements = False
            for achievement in achievements:
                if achievement["name"] == "A hairy situation":
                    has_achievement = True
            if not has_achievement:
                myDao.award_achievement(username, "A hairy situation")
                print "Awarded A hairy situation to {}".format(username)

    #change game status to complete
    myDao.set_game_status(gameid, 2)

    #remove all users from game
    users = myDao.get_game_users(gameid)
    for username in users:
        myDao.leave_game(username)
    
def is_safe(gameid, username):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    location = myDao.get_location(username)
    lat = location["lat"]
    lng = location["lng"]
    landmarks = myDao.get_landmarks(gameid)
    for landmark in landmarks:
        if landmark["type"] == 'safezone':
            if math.sqrt((lat-landmark["lat"])**2 + (lng - landmark["lng"])**2) <= LMK_RADIUS:
                return True

def award_treasure(gameid, username):
    print "Awarding treasure"
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    location = myDao.get_location(username)
    lat = location["lat"]
    lng = location["lng"]
    landmarks = myDao.get_landmarks(gameid)
    for landmark in landmarks:
        if landmark["type"] == 'treasure':
            print "Inside treasure landmark"
            print "User location-   lat: {}, lng: {}".format(lat, lng)
            print "Landmark location- lat: {}, lng: {}".format(landmark["lat"], landmark["lng"])
            if math.sqrt((lat-landmark["lat"])**2 + (lng - landmark["lng"])**2) <= LMK_RADIUS:
                print "Inside distance check"
                if landmark["landmark_id"] not in myDao.get_treasure_taken(username):
                    print "Inside hasn't taken yet"
                    items = myDao.get_treasure_item(landmark["landmark_id"])
                    for item in items:
                        itemname = myDao.get_item_info(item)["name"]
                        myDao.add_item(username, itemname)
                    print "Added items to inventory for {}".format(username)
    return

@app.route("/")
def hello():
    return "Welcome to the Wherewolf game! May the moon be ever in your favor."
    

@app.route("/v1/clearItAll", methods=["GET"])
def clearTables():
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    myDao.clear_tables()
    return "cleared tables in preparation for massive computing operations"
 
#(username, password, firstname, lastname) returns: {'status': 'success'}
#create users
@app.route('/v1/register', methods=["POST"])
def create_user():
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "success"

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    username = request.form['username']
    password = request.form['password']

    if len(password) < 5:
        response["status"] = "failure: password too short"
        print "password too short"

    try:
        myDao.create_user(username, password, firstname, lastname)
        print "user info", myDao.get_user_info(username)
    except UserAlreadyExistsException as e:
        print e
        response["status"] = "failure: user already exists"
    return jsonify(response)

    
#create games
@app.route('/v1/game', methods=["POST", "GET"])
def create_game():
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    response = {}
    response["status"] = "success"   
    if userinfo is None:
        print "no user info obtained"
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
    
    #GET
    #Get a list of current games
    if request.method == "GET":
        games = myDao.get_games()
        response = {}
        response["status"] = "success"
        response["results"] = games
        return jsonify(response)
    
    #POST
    #Add a new game
    game_name = request.form['game_name']
    description = request.form['description']
    if userinfo[2] > 0:
        print "user already in a game"
        response["status"] = "failure: user already in game"
    else:
    	response["results"] = {}
    	response["results"]["game_id"] = myDao.create_game(username, game_name, description, GAME_RADIUS, GAME_LNG, GAME_LAT)
    return jsonify(response)

#start the game
@app.route('/v1/game/<gameid>/start', methods=["PUT"])
def start_game(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    response = {}
    response["status"] = "failure"
    if userinfo is None:
        response["status"] = "failure: no user info found"
        return jsonify(response)
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
        return jsonify(response)

    gameinfo = myDao.game_info(gameid)
    if gameinfo["admin_id"] != userinfo[0]:
        response["status"] = "failure: user not game admin"
        return jsonify(response)
    
    myDao.set_game_status(gameid, 1)
    players = myDao.get_players(gameid)
    numWolves = int(.29999*len(players) + 1)
    random.shuffle(players)
    for i in range(0, numWolves):
        myDao.set_werewolf(players[i]["playerid"])
    response["status"] = "success"
    return jsonify(response)

#delete games
@app.route('/v1/game/<gameid>', methods=["DELETE", "PUT", "GET"])
def delete_game(gameid):
    print 'MADE IT TO PAGE'
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    response = {}
    response["status"] = "failure"
    if userinfo is None:
        response["status"] = "failure: no user info found"
        return jsonify(response)
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
        return jsonify(response)

    gameinfo = myDao.game_info(gameid)
    
    #DELETE
    #End the game and remove players
    if request.method == "DELETE":
        if gameinfo["admin_id"] != userinfo[0]:
            response["status"] = "failure: user not game admin"
        else:
    	    myDao.set_game_status(gameid, 2)
            users = myDao.get_game_users(gameid)
            for username in users:
                myDao.leave_game(username)
            response["status"] = "success"
        return jsonify(response)
    
    #GET
    #Get information about the game
    if request.method == "GET":
        response["status"] = "success"
        return jsonify(gameinfo)
    
    #PUT
    #Update a player's location
    #Also updates time and checks for daybreak/nightfall
    if request.method == "PUT":
        lat = request.form["latitude"]
        lng = request.form["longitude"]
        playerid = myDao.get_user_info(username)[2]
        gameid = myDao.get_player_info(playerid)["game_id"]

        #update the time and check for daybreak/nightfall
        #before = myDao.get_time(gameid).split(',')
        #myDao.set_time("current", "current", game_id)
        #after = myDao.get_time(gameid).split(',')
        #if (before[0] < DAYBREAKHOUR or before >= NIGHTFALLHOUR) and (after[0] >= DAYBREAKHOUR and after[0] < NIGHTFALLHOUR):
        #    daybreak(gameid)
        #elif (before[0] >= DAYBREAKHOUR and before[0] < NIGHTFALLHOUR) and (after[0] < DAYBREAKHOUR or after[0] >= NIGHTFALLHOUR):
        #    nightfall(gameid)

        #award items if player has entered treasure landmark
        award_treasure(gameid, username)
            
	#add stat if player is near 3 wherewolves
        if myDao.get_player_info(playerid)["is_werewolf"] == 0:
            nearby_players = myDao.get_werewolf_nearby(username, gameid, NEARBY_RADIUS)
            werewolves = 0
            for nearby_player in nearby_players:
                playerinfo = myDao.get_player_info(nearby_player["player_id"])
                if playerinfo["is_werewolf"] > 0:
                    werewolves += 1
            if werewolves >= 2:
                myDao.set_player_stat(username, 'hairy', '1')

        #update the location
        myDao.set_location(username, lat, lng)
        response["status"] = "success"
        return jsonify(response)

@app.route('/v1/game/<gameid>/exit', methods=["PUT"])
def leave_game(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    response = {}
    response["status"] = "failure"

    if userinfo is None:
        response["status"] = "failure: no user found"
    elif userinfo[1] != md5.new(password).hexdigest():
        response["status"] = "failure: incorrect password"
    elif userinfo[2] == -1:
        response["status"] = "failure: user is not in a game"
    else:
        response["status"] = "success"
        myDao.set_dead(username)
        myDao.leave_game(username)
    return jsonify(response)

@app.route('/v1/game/<gameid>/lobby', methods=["POST"])
def join_game(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    response = {}
    response["status"] = "failure"

    gameinfo = myDao.game_info(gameid)
    if userinfo is None:
        response["status"] = "failure: no user found"
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
    elif userinfo[2] > 0:
        print "user already in a game"
        response["status"] = "failure: user already in game"
    elif gameinfo["status"] == 1:
        response["status"] = "failure: game already in progress"
    elif gameinfo["status"] == 2:
        response["status"] = "failure: game has already ended"
    else:
        myDao.join_game(username, gameid)
        response["status"] = "success"
    
    return jsonify(response)

@app.route('/v1/game/<gameid>/ballot', methods=["POST", "GET"])
def cast_vote(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "failure"
    username = request.authorization['username']
    password = request.authorization['password']
    userinfo = myDao.get_user_info(username)
    gameinfo = myDao.game_info(gameid)
    hour = myDao.get_time(gameid).split(',')[0]
    hour = int(hour)
    if userinfo is None:
        print "no user info obtained"
        return jsonify(response)
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
        return jsonify(response)
    else:
        users = myDao.get_game_users(gameid)
        if not (username in users):
            response["status"] = "failure: user not in the game"
            return jsonify(response)

    #cast votes
    if request.method == "POST":
        target = request.form['player_id']
        target = myDao.get_user(target)
        if not (hour >= DAYBREAKHOUR and hour < NIGHTFALLHOUR):
            response["status"] = "failure: can't vote at night"
        else:
            myDao.vote(gameid, username, target)
            response["status"] = "success"
        return jsonify(response)

    #get vote count
    if request.method == "GET":
        votes = myDao.get_votes(gameid)
        voteList = []
        response["results"] = []
        for item in votes:
            voteList.append(vote[0])
        playerids = set(voteList)
        count = 0
        for playerid in playerids:
            response["results"].append({})
            response["results"][count]['playerid'] = playerid
            response["results"][count]['votes'] = 0
            for vote in votes:
                if vote[0] == response["results"][count]['playerid']:
                    response["results"][count]['votes'] += 1
            count += 1
        response["status"] = "success"
        return jsonify(response)

@app.route('/v1/game/<gameid>/attack', methods=["POST"])
def attack(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "failure"
    username = request.authorization['username']
    password = request.authorization['password']
    victim = request.form['username']
    userinfo = myDao.get_user_info(username)

    #check authentication
    if userinfo is None:
        print "no user info obtained"
        response["status"] = "failure: no user"
        return jsonify(response)
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
        return jsonify(response)
   
    #check in game
    users = myDao.get_game_users(gameid)
    if username not in users:
        response["status"] = "failure: user not in the game"
        return jsonify(response)
    
    #check if villager
    playerinfo = myDao.get_player_info(userinfo[2])
    if playerinfo["is_werewolf"] == 0:
        response["status"] = "failure: not a werewolf"
        return jsonify(response)

    #check if villager is safe
    if is_safe(gameid, victim):
        response["status"] = "failure: villager is in safe zone"
        return jsonify(response)
    
    #check if in cooldown period
    lastAttack = myDao.get_player_stat(username, 'last_attack')
    if lastAttack is None:
        cool = True
    else:
        lastAttack = lastAttack[2]
        lastAttack = lastAttack.split(',')
        lastAttack = lastAttack[0]*60 + lastAttack[1]
        hour = datetime.datetime.now().hour
        minute = datetime.datetime.now().minute
        time = hour*60 + minute
        if time - lastAttack > 30:
            cool = True
        elif (time - lastAttack < 0) and (time - lastAttack > -1410):
            cool = True
        else:
            cool = False
    if not cool:
        response['status'] = 'failure: still in cooldown period'
        return jsonify(response)
    myDao.set_player_stat(username, 'last_attack', myDao.get_time(gameid))
    
    villager = [VHP, VATTACK]
    items = myDao.get_items(victim)
    for item in items:
        villager[0] += ARMOR[item["a_rating"]]*item["quantity"]
        villager[1] += WEAPON[item["w_damage"]]*item["quantity"]
    level = myDao.get_player_info(userinfo[2])["is_werewolf"]
    werewolf = [WWHP[level-1], WWATTACK[level-1]]
    
    #attack sequence
    while villager[0] > 0 and werewolf[0] > 0:
        w_attack = random.randint(0, werewolf[1])
        villager[0] -= w_attack
        if villager[0] <= 0:
           break
        v_attack = random.randint(0, villager[1])
        werewolf[0] -= v_attack
    
    if villager[0] < 0:
        myDao.set_dead(victim)
        myDao.level_up(username)
        
        #check if all villagers are dead, if so end game
        players = myDao.get_players(gameid)
        stop_playing = True
        for player in players:
            if player["is_werewolf"] == 0 and player["is_dead"] == 0:
                stop_playing = False
        if stop_playing:
            end_game(gameid)

        #add kill if werewolf won
        myDao.set_player_stat(username, 'kill', '1')
	response['status'] = 'success'
        response['results'] = {'summary': 'werewolf killed villager ', 'combatant': myDao.get_user_info(victim)[2]}
    else:
        #add player_stat if player won
        myDao.set_player_stat(victim, 'survival', '1')
        response['status'] = 'success'
        response['results'] = {'summary': 'villager survived', 'combatant': myDao.get_user_info(victim)[2]}

    return jsonify(response)

@app.route('/v1/game/<gameid>/nearby', methods=["GET"])
def get_nearby(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "failure"
    username = request.authorization["username"]
    password = request.authorization["password"]
    #check authentication
    if userinfo is None:
        print "no user info obtained"
        response["status"] = "failure: no user"
        return jsonify(response)
    elif userinfo[1] != md5.new(password).hexdigest():
        print "incorrect password"
        response["status"] = "failure: incorrect password"
        return jsonify(response)
    
    alive_nearby = myDao.get_alive_nearby(username, gameid, NEARBY_RADIUS)
    response["status"] = "success"
    response["results"] = alive_nearby
    return jsonify(response)

@app.route('/v1/game/<gameid>/time', methods=["POST"])
def set_time(gameid):
    myDao = get_dao('wherewolf', 'juliette', 'eiffel78')
    response = {}
    response["status"] = "failure"
    check = myDao.get_time(gameid)
    if len(check) > 1:
        before = myDao.get_time(gameid).split(',')
        print "Before time: ", before
        before[0] = int(before[0])
        before[1] = int(before[1])
    hour = request.form['game_hour']
    minute = request.form['game_minute']
    myDao.set_time(hour, minute, gameid)
    if len(check) > 1:
        after = myDao.get_time(gameid).split(',')
        after[0] = int(after[0])
        after[1] = int(after[1])
        if (before[0] < DAYBREAKHOUR or before >= NIGHTFALLHOUR) and (after[0] >= DAYBREAKHOUR and after[0] < NIGHTFALLHOUR):
            daybreak(gameid)
        elif (before[0] >= DAYBREAKHOUR and before[0] < NIGHTFALLHOUR) and (after[0] < DAYBREAKHOUR or after[0] >= NIGHTFALLHOUR):
            nightfall(gameid)
    response["status"] = "success"
    return jsonify(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
