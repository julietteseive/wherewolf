from flask import Flask, request, jsonify
import psycopg2
app = Flask(__name__)

def get_db(databasename='tasklist', 
        username='postgres',
        password='furry'):
    return psycopg2.connect(database= databasename,
             user=username, password=password)

@app.route("/")
def hello():
    return "Hello World!"

def check_password(username, password):
    print 'checking password for {}'.format(username)
    conn = get_db()
    with conn:
        sql = ('select password from taskuser '
               'where username=%s')
        cur = conn.cursor()
        cur.execute(sql, (username, ))
        dbpass = cur.fetchone()[0]
        print '----------------{}'.format(dbpass)
        if dbpass == password:
            return True
        else:
            return False
    
@app.route('/user', methods=["POST"])
def create_user():
    conn = get_db()
    username = request.form['username']
    password = request.form['password']
    sqlquery = ('select username from taskuser '
                'where username=%s')
    sql = ('insert into taskuser (username, password) '
        'values (%s, %s)')
    cur = conn.cursor()
    response = {}
    response["status"] = "failure"
    cur.execute(sqlquery, (username,))
    if not cur.fetchone():
        cur.execute(sql, (username, password))
        conn.commit()
        print 'created a user called {}'.format(username)
        response["status"] = "success"
        response["username"] = username
        # MIME application/json
    
    return jsonify(response)
    
@app.route('/tasks')
def get_tasks():
    return 'My tasks\n'
    
@app.route('/tasks', methods=["POST"])
def add_task():
    conn = get_db()
    description = request.form["description"]
    category = request.form["category"]
    priority = request.form["priority"]
    auth = request.authorization
    username = auth.username
    password = auth.password
    response = {}
    response["status"] = "failure"
    if check_password(username, password):
        insertsql = ('insert into tasks ( '
                     'description, category, priority) '
                     'values (%s, %s, %s)')
        with conn:
            cur = conn.cursor()
            cur.execute(insertsql, (description,
                                    category,
                                    int(priority)))
            conn.commit()
            response["status"] = "success"
            
    return jsonify(response)

if __name__ == "__main__":
    app.run(debug=True)