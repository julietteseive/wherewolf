-- schema for the Wherewolf game


DROP EXTENSION IF EXISTS cube CASCADE;
DROP EXTENSION IF EXISTS earthdistance CASCADE;
DROP INDEX IF EXISTS username CASCADE;
DROP INDEX IF EXISTS playerindex CASCADE;
DROP INDEX IF EXISTS itemname CASCADE;
DROP INDEX IF EXISTS pos_index CASCADE;
DROP TABLE IF EXISTS player CASCADE;
DROP TABLE IF EXISTS gameuser CASCADE;
DROP TABLE IF EXISTS game CASCADE;
DROP TABLE IF EXISTS landmark CASCADE;
DROP TABLE IF EXISTS achievement CASCADE;
DROP TABLE IF EXISTS user_achievement CASCADE;
DROP TABLE IF EXISTS item CASCADE;
DROP TABLE IF EXISTS inventory CASCADE;
DROP TABLE IF EXISTS treasure CASCADE;
DROP TABLE IF EXISTS player_stat CASCADE;
DROP TABLE IF EXISTS user_stat CASCADE;
DROP TABLE IF EXISTS vote CASCADE;
DROP TABLE IF EXISTS treasure_taken CASCADE;

CREATE EXTENSION cube;
CREATE EXTENSION earthdistance;

CREATE TABLE gameuser (
	user_id 	SERIAL PRIMARY KEY,
	firstname	VARCHAR(80) NOT NULL,
	lastname	VARCHAR(80) NOT NULL,
	created_at	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	username	VARCHAR(80) UNIQUE NOT NULL,
	password	VARCHAR(128) NOT NULL,
	current_player  INTEGER DEFAULT -1
);

CREATE TABLE game (
	game_id 	SERIAL PRIMARY KEY,
	admin_id 	INTEGER NOT NULL REFERENCES gameuser,
	status 		INTEGER NOT NULL DEFAULT 0,
	name		varchar(80) NOT NULL,
	description	TEXT,
	game_time   	TEXT DEFAULT '-1',
        game_radius     FLOAT NOT NULL,
        game_lat        FLOAT NOT NULL,
        game_lng        FLOAT NOT NULL
);

CREATE TABLE player (
   	player_id	SERIAL PRIMARY KEY,
   	is_dead		INTEGER NOT NULL,
  	lat		FLOAT	NOT NULL,
    	lng		FLOAT	NOT NULL,
	is_werewolf	INTEGER NOT NULL DEFAULT 0,
	num_gold	INTEGER NOT NULL DEFAULT 0,
	game_id		INTEGER REFERENCES game
);


-----------create table for points of interest
CREATE TABLE landmark (
	landmark_id	serial primary key,
	lat		float NOT NULL,
	lng		float NOT NULL,
	radius		float NOT NULL,
	type		TEXT NOT NULL,
	game_id		INTEGER NOT NULL REFERENCES game,
	is_active 	INTEGER NOT NULL DEFAULT 0,
	created_at	TIMESTAMP
);


CREATE TABLE achievement (
	achievement_id	SERIAL PRIMARY KEY,
	name		VARCHAR(80) NOT NULL,
	description	TEXT NOT NULL
);


CREATE TABLE user_achievement (
	user_id		INTEGER references gameuser,
	achievement_id	INTEGER references achievement,
	created_at	TIMESTAMP
);


CREATE TABLE item (
	itemid 		SERIAL PRIMARY KEY,
	name 		VARCHAR(80) NOT NULL,
	description 	TEXT,
        w_damage        TEXT DEFAULT 'unarmed',
        a_rating        TEXT DEFAULT 'unarmored'
);


CREATE TABLE inventory (
	playerid 	INTEGER REFERENCES player,
	itemid 		INTEGER REFERENCES item,
	quantity 	INTEGER,
	primary key (playerid, itemid)
);

CREATE TABLE treasure_taken (
        playerid        INTEGER REFERENCES player,
        landmark_id     INTEGER REFERENCES landmark,
        primary key (playerid, landmark_id)
);

CREATE TABLE treasure(
	landmark_id	INTEGER REFERENCES landmark,
	item_id	 	INTEGER REFERENCES item,
	quantity  	INTEGER NOT NULL DEFAULT 1,
	primary key (landmark_id, item_id)
);

-- used to store number of kills in a game --
CREATE TABLE player_stat (
	player_id 	INTEGER NOT NULL REFERENCES player,
	stat_name	VARCHAR(80) NOT NULL,
	stat_value	VARCHAR(80) NOT NULL,
        quantity        INTEGER DEFAULT 1,
        primary key (player_id, stat_name)
);

-- used to store number of kills historically
CREATE TABLE user_stat (
	user_id 	INTEGER NOT NULL,
	stat_name	VARCHAR(80) NOT NULL,
	stat_value	VARCHAR(80) NOT NULL,
        primary key (user_id, stat_name)
);

CREATE TABLE vote (
       vote_id		SERIAL PRIMARY KEY,
       game_id          INTEGER REFERENCES game,
       player_id        INTEGER REFERENCES player,
       target_id  	INTEGER REFERENCES player,
       is_current       INTEGER DEFAULT 1,
       cast_date	TIMESTAMP
);


CREATE INDEX playerindex ON inventory(playerid);
CREATE INDEX username ON gameuser(username);
CREATE INDEX indexitemname ON item(name);
CREATE INDEX pos_index ON player USING gist (ll_to_earth(lat, lng));
-- insert some data

-- functions


/*INSERT INTO gameuser (user_id, firstname, lastname, created_at, username, password) VALUES (1, 'Robert', 'Dickerson', timestamp '2004-10-19' , 'rfdickerson', 'be121740bf988b2225a313fa1f107ca1');
INSERT INTO gameuser (user_id, firstname, lastname, created_at, username, password) VALUES (2, 'Abraham', 'Van Helsing', timestamp '2012-8-20 10:23:12', 'vanhelsing', 'be121740bf988b2225a313fa1f107ca1');

INSERT INTO game (admin_id, status, name) VALUES (1, 0, 'TheGame');

INSERT INTO player (player_id, is_dead, lat, lng, game_id) VALUES (1, 0, 38, 78, 1);
INSERT INTO player (player_id, is_dead, lat, lng, game_id) VALUES (2, 0, 38.01, 77.01, 1);

UPDATE gameuser SET current_player=1 WHERE username='rfdickerson'; 
UPDATE gameuser SET current_player=2 WHERE username='vanhelsing'; */

INSERT INTO achievement VALUES (1, 'Hair of the dog', 'Survive an attack by a werewolf');
INSERT INTO achievement VALUES (2, 'Leader of the pack', 'Finish the game as a werewolf and receive the top number of kills');
INSERT INTO achievement VALUES (3, 'Children of the moon', 'Stay alive and win the game as a werewolf');
INSERT INTO achievement VALUES (4, 'It is never Lupus', 'Vote someone to be a werewolf, when they were a townsfolk');
INSERT INTO achievement VALUES (5, 'A hairy situation', 'Been near 3 werewolves at once.');
INSERT INTO achievement VALUES (6, 'Call in the Exterminators', 'Kill off all the werewolves in the game');

/*INSERT INTO user_achievement (user_id, achievement_id, created_at) VALUES (1, 1, timestamp '2014-06-06 01:01:01');
INSERT INTO user_achievement (user_id, achievement_id, created_at) VALUES (1, 2, timestamp '2014-08-08 03:03:03');*/

INSERT INTO item VALUES (1, 'Wolfsbane Potion', 'Protects the drinker from werewolf attacks', 'unarmed', 'unarmored');
INSERT INTO item VALUES (2, 'Blunderbuss', 'A muzzle-loading firearm with a short, large caliber barrel.', 'medium', 'unarmored');
INSERT INTO item VALUES (3, 'Invisibility Potion', 'Makes the imbiber invisible for a short period of time.', 'unarmed', 'unarmored');
INSERT INTO item VALUES (4, 'Silver Knife', 'A blade made from the purest of silvers', 'light', 'unarmored');
INSERT INTO item VALUES (5, 'Dragon Skin', 'The thickest of skins', 'unarmed', 'heavy')

/*INSERT INTO inventory VALUES (1, 2, 1);
INSERT INTO inventory VALUES (2, 1, 1);*/