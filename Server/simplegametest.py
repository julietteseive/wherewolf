import requests
import json
from wherewolfdao import WherewolfDao

hostname = "http://localhost:5000"
rest_prefix = "/v1"
gameid = 0

def get_current_votes(game_id):
    myDao = get_dao()
    votes = myDao.get_votes(game_id)
    return votes

def get_alive_nearby(username, game_id):
    myDao = get_dao()
    alive_nearby = myDao.get_alive_nearby(username, game_id, 500000)
    return alive_nearby

def get_achievements(username):
    myDao = get_dao()
    achievements = myDao.get_achievements(username)
    return achievements

def get_players(game_id):
    myDao = get_dao()
    players = myDao.get_players(game_id)
    return players

def get_users(game_id):
    myDao = get_dao()
    players = get_players(game_id)
    users = []
    for player in players:
        users.append(myDao.get_user(player["playerid"]))
    return users

def attack(username, vict_username, game_id):
    url = hostname + rest_prefix + "/game/" + str(game_id) + "/attack"
    payload = {"username": vict_username}
    r = requests.post(url, auth = (username, 'paper'), data=payload)
    response = r.json()
    print response["status"]
    if response["status"] == "success":
        print response["results"]

def get_items(username):
    myDao = get_dao()
    items = myDao.get_items(username)
    return items

def get_villagers(game_id):
    myDao = get_dao()
    gameusers = myDao.get_game_users(game_id)
    villagers = []
    for gameuser in gameusers:
        playerid = get_playerid(gameuser)
        if myDao.get_player_info(playerid)["is_werewolf"] == 0:
            villagers.append(gameuser)
    return villagers

def get_werewolves(game_id):
    myDao = get_dao()
    gameusers = myDao.get_game_users(game_id)
    werewolves = []
    for gameuser in gameusers:
        playerid = get_playerid(gameuser)
        if myDao.get_player_info(playerid)["is_werewolf"] > 0:
            werewolves.append(gameuser)
    return werewolves

def update_location(username, game_id, lat, lng):
    url = hostname + rest_prefix + '/game/' + str(game_id)
    payload = {'latitude': lat, 'longitude': lng}
    r = requests.put(url, auth=(username, 'paper'), data=payload)
    print "{} has moved to lat: {}, lng: {}".format(username, lat, lng)

def make_rich(username, game_id):
    landmarks = get_landmarks(game_id)
    lat = 0
    lng = 0
    for landmark in landmarks:
        if landmark["type"] == 'treasure':
            lat = landmark["lat"]
            lng = landmark["lng"]
    print "Treasure zone located at lat: {}, lng: {}".format(lat, lng, username)
    update_location(username, game_id, lat, lng)

def make_safe(username, game_id):
    landmarks = get_landmarks(game_id)
    lat = 0
    lng = 0
    for landmark in landmarks:
        if landmark["type"] == 'safezone':
            lat = landmark["lat"]
            lng = landmark["lng"]
    print "Safezone located at lat: {}, lng: {}".format(lat, lng)
    update_location(username, game_id, lat, lng)

def get_landmarks(game_id):
    myDao = get_dao()
    landmarks = myDao.get_landmarks(game_id)
    return landmarks

def get_dao():
    myDao = WherewolfDao('wherewolf', 'postgres', 'Karkwan1.')
    return myDao

def create_user(username, password, firstname, lastname):
    payload = {'username': username, 'password': password, 'firstname': firstname, 'lastname': lastname}
    url = "{}{}{}".format(hostname, rest_prefix, "/register")
    r = requests.post(url, data=payload)
    print "Creating user {} with password {}".format(username, password)
    response = r.json()
    print response["status"]

def create_game(username, password, gamename, description):
    payload = {'game_name': gamename, 'description': description}
    url = "{}{}{}".format(hostname, rest_prefix, "/game")
    r = requests.post(url, auth=(username, password), data=payload)
    print "Creating game with admin {}".format(username)
    response = r.json()
    print response["status"]
    return response["results"]["game_id"]

def join_game(username, password, game_id):
    url = "{}{}{}{}".format(hostname, rest_prefix, "/game/" + str(game_id), "/lobby")
    r = requests.post(url, auth=(username, password))
    response = r.json()
    print "User {} joining game with id {}".format(username, game_id)
    print response["status"]

def start_game(username, password, game_id):
    url = hostname + rest_prefix + "/game/" + str(game_id) + "/start"
    r = requests.put(url, auth=(username, password))
    response = r.json()
    print "User {} starting game with id {}".format(username, game_id)
    print response["status"]

def leave_game(username, password, game_id):
    url = hostname + rest_prefix + "/game/" + str(game_id) + "/exit"
    r = requests.put(url, auth=(username, password))
    response = r.json()
    print "User {} leaving game with id {}".format(username, game_id)
    print response["status"]

def delete_game(username, password, game_id):
    url = hostname + rest_prefix + "/game/" + str(game_id)
    r = requests.delete(url, auth=(username, password))
    response = r.json()
    print "User {} deleting game with id {}".format(username, game_id)
    print response["status"]

def cast_vote(username, password, game_id, vote):
    url = hostname + rest_prefix + "/game/" + str(game_id) + "/ballot"
    payload = {"player_id": vote}
    r = requests.post(url, auth=(username, password), data=payload)
    response = r.json()
    #print "User {} casting vote for playerid {}".format(username, vote)
    #print response["status"]

def get_playerid(username):
    url = hostname + rest_prefix + "/test/playerid"
    payload = {"username": username}
    r = requests.post(url, data=payload)
    response = r.json()
    playerid = response["results"]
    return playerid

def set_time(hour, minute, game_id):
    print "Setting time to hour: {}  minute: {}".format(hour, minute)
    url = hostname + rest_prefix + '/game/' + str(game_id) + '/time'
    payload = {"game_hour": hour, "game_minute": minute}
    r = requests.post(url, data=payload)
    response = r.json()
    print response["status"]

def cast_votes(game_id, vote):
    cast_vote('michael', 'paper', game_id, vote)
    cast_vote('dwight', 'paper', game_id, vote)
    cast_vote('jim', 'paper', game_id, vote)
    cast_vote('pam', 'paper', game_id, vote)
    cast_vote('ryan', 'paper', game_id, vote)
    cast_vote('andy', 'paper', game_id, vote)
    cast_vote('angela', 'paper', game_id, vote)
    cast_vote('toby', 'paper', game_id, vote)

def create_users():
    create_user('michael', 'paper', 'Michael', 'Scott')
    create_user('dwight', 'paper', 'Dwight', 'Schrute')
    create_user('jim', 'paper', 'Jim', 'Halpert')
    create_user('pam', 'paper', 'Pam', 'Beesly')
    create_user('ryan', 'paper', 'Ryan', 'Howard')
    create_user('andy', 'paper', 'Andy', 'Bernard')
    create_user('angela', 'paper', 'Angela', 'Martin')
    create_user('toby', 'paper', 'Toby', 'Flenderson')

def join_game_all():
    join_game('dwight', 'paper', gameid)
    join_game('jim', 'paper', gameid)
    join_game('pam', 'paper', gameid)
    join_game('ryan', 'paper', gameid)
    join_game('andy', 'paper', gameid)
    join_game('angela', 'paper', gameid)
    join_game('toby', 'paper', gameid)


    

if __name__ == "__main__":
    print
    print
    print
    print "Clearing necessary database tables to prepare for test..."
    requests.get(hostname + rest_prefix + "/clearItAll")
    print
    print
    print
    #First game
    print "Creating game users..."
    create_users()
    print
    print "Creating game with michael as admin..."
    gameid = create_game('michael', 'paper', 'AAWOOOOOOOOOOO', 'The game to end all games')
    print
    print "All users joining game..."
    join_game_all()
    print
    print "User toby leaving game..."
    leave_game('toby', 'paper', gameid)
    print
    print "Game admin michael deleting game..."
    delete_game('michael', 'paper', gameid)
    
    #Second game
    print
    print "Creating a new game with michael as admin..."
    gameid = create_game('michael', 'paper', 'Furry Fury', 'Fury in the night.')
    print
    print "All user joining game..."
    join_game_all()
    print
    print "User michael starting the game..."
    start_game('michael', 'paper', gameid)
    print "Setting initial game time..."
    set_time(8, 8, gameid)
    print
    print
    print "Villagers for this game are: ", get_villagers(gameid)
    print "Werewolves for this game are: ", get_werewolves(gameid)
    print
    danger_crew = get_werewolves(gameid)
    safety_captain = get_villagers(gameid)[0]
    print "Players casting votes..."
    print "Checking current ballot status..."
    playerid = get_playerid(danger_crew[2])
    cast_votes(gameid, playerid)
    votes = get_current_votes(gameid)
    for vote in votes:
        if vote[2] == 1:
            print "Vote for player:", vote[0], "Vote by player:", vote[1]
    print "Voting to remove user {}...".format(danger_crew[2])

    
    #First night
    print
    print "Setting time to night..."
    set_time(20, 1, gameid)
    print
    print "Moving villager {} to a safezone...".format(safety_captain)
    make_safe(safety_captain, gameid)
    print
    print "Moving 3 werewolves {}, {}, and {} nearby villager {}".format(danger_crew[0], danger_crew[1], danger_crew[2], safety_captain)
    for werewolf in danger_crew:
        make_safe(werewolf, gameid)
    print
    print "Moving villager {} to a treasure zone...".format(safety_captain)
    make_rich(safety_captain, gameid)
    print "Moving villager {} out of treasure zone...".format(safety_captain)
    update_location(safety_captain, gameid, 0, 0)
    print
    print "Checking for items gathered in treasure zone by {}".format(safety_captain)
    items = get_items(safety_captain)
    item = items
    print "Items in inventory: "
    for items in item:
       print "Name:", items["name"]
       print "Description:",items["description"]
       print "Quantity:",items["quantity"]
    print
    print "Moving werewolf {} close to {} for attack".format(danger_crew[0], safety_captain)
    update_location(danger_crew[0], gameid, 10, 10)
    alive_nearby = get_alive_nearby(danger_crew[0], gameid)
    print "Retrieving list of nearby villagers..."
    for villager in alive_nearby:
        print "Player_id: {}".format(villager["player_id"])
        print "Distance: {}".format(villager["distance"])
        print
    print "{} attacking {}".format(danger_crew[0], safety_captain)
    attack(danger_crew[0], safety_captain, gameid)
    
    #Second day
    print
    print "Setting time to day..."
    set_time(10, 15, gameid)
    print "Casting votes..."
    print "Voting for player {}...".format(danger_crew[1])
    playerid = get_playerid(danger_crew[1])
    cast_votes(gameid, playerid)
    
    #Second night
    print
    print "Setting time to night..."
    set_time(20, 34, gameid)
    
    #Third day
    print
    print "Setting time to day..."
    set_time(7, 11, gameid)
    villagers = get_villagers(gameid)
    print "Casting votes..."
    print "Voting for player {}...".format(villagers[1])
    playerid = get_playerid(villagers[1])
    cast_votes(gameid, playerid)


    #Third night
    print
    print "Setting time to night..."
    set_time(22, 10, gameid)
    
    #Fourth day
    print
    print "Setting time to day..."
    set_time(9, 17, gameid)
    print "Casting votes..."
    print "Voting for player {}...".format(danger_crew[0])
    playerid = get_playerid(danger_crew[0])
    cast_votes(gameid, playerid)

    users = get_users(gameid)
    #Fourth night
    print
    print "Setting time to night..."
    set_time(20, 1, gameid)
    print
    print "All werewolves killed...game automatically ending..."
    print
    print "Achievements awarded: "
    for user in users:
        print "{}:".format(user),
        for achievement in get_achievements(user):
            print "\t" + achievement["name"]